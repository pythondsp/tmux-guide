TMUX commands
*************


Installation
============

.. code-block:: shell

    $ sudo apt-get install tmux


.tmux.conf
==========

.. note:: 

    First, create a file '.tmux.conf' in the 'home' folder. Here, The command 'ctrl-b' is replaced with 'crtl-a' along with some other changes.

.. code-block:: text

    # ~/.tmux.conf

    # unbind default prefix and set it to ctrl-a
    unbind C-b
    set -g prefix C-a
    bind C-a send-prefix

    # make delay shorter
    set -sg escape-time 0



    #### key bindings ####

    # reload config file
    bind r source-file ~/.tmux.conf \; display ".tmux.conf reloaded!"

    # quickly open a new window
    bind N new-window

    # synchronize all panes in a window
    bind y setw synchronize-panes

    # pane movement shortcuts (same as vim)
    bind h select-pane -L
    bind j select-pane -D
    bind k select-pane -U
    bind l select-pane -R

    # enable mouse support for switching panes/windows
    set -g mouse-utf8 on
    set -g mouse on


    #### copy mode : vim ####

    # set vi mode for copy mode
    setw -g mode-keys vi

    # copy mode using 'Esc'
    unbind [
    bind Escape copy-mode

    # start selection with 'space' and copy using 'y'
    bind -t vi-copy 'y' copy-selection

    # paste using 'p'
    unbind p
    bind p paste-buffer


Basic
=====


.. note:: 

    Below commands can be used in terminal for various 'tmux' operations e.g. start and stop etc., 

    .. code-block:: shell
    
        $ tmux


+-----------------------------+---------------------------------------+
| Command                     | Description                           |
+=============================+=======================================+
| tmux                        | start tmux                            |
+-----------------------------+---------------------------------------+
| tmux new -s <name>          | start tmux with <name>                |
+-----------------------------+---------------------------------------+
| tmux ls                     | shows the list of sessions            |
+-----------------------------+---------------------------------------+
| tmux a \#                   | attach the detached-session           |
+-----------------------------+---------------------------------------+
| tmux a -t <name>            | attach the detached-session to <name> |
+-----------------------------+---------------------------------------+
| tmux kill-session –t <name> | kill the session <name>               |
+-----------------------------+---------------------------------------+
| tmux kill-server            | kill the tmux server                  |
+-----------------------------+---------------------------------------+



.. note:: 

    Press 'ctrl-a' and then type the commands from the below tables, 


Help
====

+---------+---------------------------+
| Command | Description               |
+=========+===========================+
| \?      | show the list of commands |
+---------+---------------------------+

Reload
======

+---------+------------------------+
| Command | Description            |
+=========+========================+
| r       | reload .tmux.conf file |
+---------+------------------------+

Sessions
========

+---------+-----------------+
| Command | Description     |
+=========+=================+
| s       | list sessions   |
+---------+-----------------+
| \$      | rename sessions |
+---------+-----------------+
| d       | detach session  |
+---------+-----------------+



Windows
=======

.. note:: 

    ctrl-d can be used to close the window or panes **without** using ctrl-a.

+---------+-----------------------------+
| Command | Description                 |
+=========+=============================+
| w       | list windows and select one |
+---------+-----------------------------+
| \,      | rename window               |
+---------+-----------------------------+
| c or N  | create new window           |
+---------+-----------------------------+
| n       | go to next window           |
+---------+-----------------------------+
| p       | go to previous window       |
+---------+-----------------------------+
| f       | find window                 |
+---------+-----------------------------+
| \&      | kill window                 |
+---------+-----------------------------+
| 0-9     | go to window 0-9            |
+---------+-----------------------------+


Panes (split windows)
=====================


+------------+------------------------------------------+
| Command    | Description                              |
+============+==========================================+
| %          | vertical split                           |
+------------+------------------------------------------+
| \'\'       | horizontal split                         |
+------------+------------------------------------------+
| x          | kill pane                                |
+------------+------------------------------------------+
| o          | go to next pane                          |
+------------+------------------------------------------+
| h, j, k, l | go to next pane in vim-style             |
+------------+------------------------------------------+
| z          | toggle full-screen mode for current pane |
+------------+------------------------------------------+
| arrow keys | resize the pane                          |
+------------+------------------------------------------+
| q          | show pane-numbers                        |
+------------+------------------------------------------+


Commands
========

+---------+--------------------+
| Command | Description        |
+=========+====================+
| \:      | go to command mode |
+---------+--------------------+

Next, type the commands as below, 

+---------------------+------------------------------+
| Command             | Description                  |
+=====================+==============================+
| list-keys           | shows all the commands       |
+---------------------+------------------------------+
| list-panes          | shows the names of all panes |
+---------------------+------------------------------+
| resize-pane -D 20   | resize down                  |
+---------------------+------------------------------+
| resize-pane -U 20   | resize up                    |
+---------------------+------------------------------+
| resize-pane -L 20   | resize left                  |
+---------------------+------------------------------+
| resize-pane -R 20   | resize right                 |
+---------------------+------------------------------+
| swap-pane -s 3 -t 1 | swap pane '1' with pane '3'  |
+---------------------+------------------------------+


Copy mode
=========

'Vim' mode is set in the .tmux.conf file and 'Esc' is used to enter into the 'copy mode',

+---------+-----------------+
| Command | Description     |
+=========+=================+
| Esc     | go to copy mode |
+---------+-----------------+

.. note:: 

    Next use following commands **without** using 'ctrl-a'.


+------------+----------------------------------------+
| Command    | Description                            |
+============+========================================+
| q          | quit mode                              |
+------------+----------------------------------------+
| j, k, l, h | down, up, right left                   |
+------------+----------------------------------------+
| J or K     | scroll down or up                      |
+------------+----------------------------------------+
| F or B     | go to next or previous page            |
+------------+----------------------------------------+
| \$         | go to end of line                      |
+------------+----------------------------------------+
| \0         | go to beginning of line                |
+------------+----------------------------------------+
| w or b     | go to  next or previous word           |
+------------+----------------------------------------+
| / or \?    | search forward or backward             |
+------------+----------------------------------------+
| n          | search next (use after above commands) |
+------------+----------------------------------------+
| space      | start selection                        |
+------------+----------------------------------------+
| Esc        | clear selection                        |
+------------+----------------------------------------+
| y          | copy section                           |
+------------+----------------------------------------+


.. note:: 

    Use below command with 'ctrl-a' for paste-operation.

+---------+-----------------+
| Command | Description     |
+=========+=================+
| p       | paste selection |
+---------+-----------------+



